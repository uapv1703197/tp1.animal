package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

public class Animal_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        Intent intent = getIntent();
        if (intent != null){
            String str = "";
            if (intent.hasExtra("pos")){
                str = intent.getStringExtra("pos");
            }

            final Animal toto = AnimalList.getAnimal(str);

            //TITRE


            final TextView TextView = (TextView) findViewById(R.id.nom);
            TextView.setText(str);

            // Image

            final ImageView ImageView = (ImageView) findViewById(R.id.img);
            ImageView.setImageResource(getResources().getIdentifier(toto.getImgFile(), "drawable", getPackageName()));



            // INFO textView

            String esperance = toto.getStrHightestLifespan();
            String gestation = toto.getStrGestationPeriod();
            String poidsN = toto.getStrBirthWeight();
            String poidsA = toto.getStrAdultWeight();

            final TextView test1 = (TextView) findViewById(R.id.tv1);
            test1.setText(esperance);

            final TextView test2 = (TextView) findViewById(R.id.tv2);
            test2.setText(gestation);

            final TextView test3 = (TextView) findViewById(R.id.tv3);
            test3.setText(poidsN);

            final TextView test4 = (TextView) findViewById(R.id.tv4);
            test4.setText(poidsA);

            final TextInputEditText test5 = (TextInputEditText) findViewById(R.id.tv5);
            test5.setText(toto.getConservationStatus());


            // BOUTON

            Button btn = (Button) findViewById(R.id.svg);
            btn.setOnClickListener(new View.OnClickListener(){
                @Override
                public  void onClick(View v)
                {
                    toto.setConservationStatus(test5.getText().toString());
                    finish();
                }
            });





        }




    }
}
