package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listview = (ListView) findViewById(R.id.tv);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,AnimalList.getNameArray());
        listview.setAdapter(adapter);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // Do something in response to the click
                //final String item = (String) parent.getItemAtPosition(position);
                //Toast.makeText(MainActivity.this, "You selected: " + item, Toast.LENGTH_LONG).show();

                String m = (String) parent.getItemAtPosition(position);
                Intent myintent = new Intent(v.getContext(),Animal_Activity.class);
                myintent.putExtra("pos",m);
                startActivityForResult(myintent,0);
            }
        });








    }




}
